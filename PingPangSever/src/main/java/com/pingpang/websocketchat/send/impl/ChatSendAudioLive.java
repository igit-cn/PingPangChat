package com.pingpang.websocketchat.send.impl;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.redis.RedisPre;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ChatSendAudioLive extends ChatSend {

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
       
		    Set<String> msg=new HashSet<String>();
		    msg.add("regist");//添加直播间
			msg.add("query");//查询所有直播间信息
			msg.add("close");//关闭直播间
			msg.add("in");//进入直播间  暂用group处理
			msg.add("out");//离开直播间 暂用group处理
			if(!msg.contains(message.getMsg())){
				logger.info("IP:{},用户:{},对方用户:{}多媒体错误信息{}",ctx.channel().remoteAddress(),message.getFrom().getUserCode(),message.getAccept().getUserCode(),message.getMsg());
				return;
			}
			
			//注册信息
			if("regist".equals(message.getMsg())) {
				ChartUser cu=this.userService.getUser(message.getFrom());
				cu.setUserStatus("");
				cu.setLiveType("0");//webrtc 方式
				this.redisService.addSet(RedisPre.AUDIO_LIVE_SET+message.getFrom().getUserCode(),cu);
				return;
			}
			
			//注册信息
			if("query".equals(message.getMsg())) {
				Set<String> liveSet=this.redisService.getSetPrex(RedisPre.AUDIO_LIVE_SET+"*");
				Set<ChartUser> cuLiveSet=new HashSet<ChartUser>();
				for(String str:liveSet) {
					ChartUser cu=new ChartUser();
					cu.setUserCode(str.substring(RedisPre.AUDIO_LIVE_SET.length()));
					
					/**
					 * 这里做的有点不合理 因为要区分是webrtc过来还是flv过来得暂且先这样
					 */
					Set<ChartUser> liveUserSet=(Set<ChartUser>)(Set<?>)this.redisService.getSet(RedisPre.AUDIO_LIVE_SET+cu.getUserCode());
                    for(ChartUser currentCu: liveUserSet) {
                    	if(currentCu.getUserCode().equals(cu.getUserCode())) {
                    		cuLiveSet.add(currentCu);  
                    		break;
                    	}
                    }
				    	
				}
				
				message.setChatSet(cuLiveSet);
				ctx.channel().writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
				return;
			}
			
			
			//进入直播间
			if("in".equals(message.getMsg()) || "out".equals(message.getMsg()) || "close".equals(message.getMsg())) {
				if(StringUtil.isNUll(message.getGroup().getGroupCode())) {
					return;
				}
				ChartUser cu=this.userService.getUser(message.getFrom());
				cu.setUserStatus("");
				
				if("in".equals(message.getMsg())) {
				  this.redisService.addSet(RedisPre.AUDIO_LIVE_SET+message.getGroup().getGroupCode(),cu);
				}
				
				message.setFrom(cu);
				Set<ChartUser> liveUserSet=(Set<ChartUser>)(Set<?>)this.redisService.getSet(RedisPre.AUDIO_LIVE_SET+message.getGroup().getGroupCode());
				
				//获取数据后清理直播间
				if("close".equals(message.getMsg())) {
					this.redisService.deletePrex(RedisPre.AUDIO_LIVE_SET+message.getFrom().getUserCode());
				}
				
				for(ChartUser liveCu : liveUserSet) {
					liveCu.setUserPassword("");
					message.setAccept(userService.getUser(liveCu));// 添加接收方数据
					
					if("in".equals(message.getMsg())) {
						message.setMsg(message.getFrom().getUserName()+"("+message.getFrom().getUserCode()+")进入直播间");
					}else if("out".equals(message.getMsg())) {
						message.setMsg(message.getFrom().getUserName()+"("+message.getFrom().getUserCode()+")离开直播间");
					}else if("close".equals(message.getMsg())) {
						message.setMsg(message.getFrom().getUserName()+"("+message.getFrom().getUserCode()+")直播结束");
					}
					
					if (ChannelManager.isExitChannel(liveCu)) {
						ChannelManager.getChannel(liveCu.getUserCode())
								.writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
					}else if(!ChannelManager.isExitChannel(liveCu)) {
						this.sendOtherServerMsg(message);
					}
				}
				return;
			}
	}
}
