package com.pingpang.websocketchat.send.impl;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.redis.RedisPre;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChatType;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * 绑定操作
 * @author dell
 */
public class ChatSendGetOldMsg extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
        if(null==message || !ChatType.QUERY_OLD_MSG.equals(message.getCmd())) {
        	return;
        }else {
        	if(null==message.getGroup() || StringUtil.isNUll(message.getGroup().getGroupCode())) {
        		message.setCmd("3");//单聊
        	}else {
        		message.setCmd("4");//群聊
        	}
        	
        	List<Message> userOldList=this.userMsgService.getUserOldMsg(message);
        	message.setOldMsg(userOldList);
        	message.setCmd(ChatType.QUERY_OLD_MSG);
        	ctx.channel().writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));		
        }
	}

}
